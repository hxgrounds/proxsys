# Proxsys library

This is a simple library to create a proxy, intermediate layer, between haxe system methods (like Sys) and client code.

The main purpose is to make easier test some parts of the code, without depending on system methods.

## Currently supported functions

### Sys functions

The `proxsys.System` interface abstracts functions defined by [Sys](https://api.haxe.org/Sys.html) class.

`proxsys.StdSystem` implements these functions using Sys methods,
while `proxsys.FakeSystem` is a fake implementation which can be used for tests.

Currently supported functions:

**Subprocess**: ✓

* [x] command(cmd:String, ?args:Array<String>):Int;
* [x] startProcess(cmd:String, ?args:Array<String>):ProcessInstance;

**env**:

* [x] getEnv(s:String):String;
* [x] putEnv(s:String, v:Null<String>):Void;
* [x] environment():Map<String, String>

**cwd**: ✓

* [x] getCwd():String;
* [x] setCwd(newDir:String):Void;

**I/O**:

* [x] print(s:String):Void;
* [x] println(s:String):Void;
* [ ] getChar(echo:Bool):Int
* [ ] stderr():Output
* [ ] stdin():Input
* [ ] stdout():Output

**time**:

* [ ] cpuTime():Float
* [ ] time():Float
* [ ] sleep(seconds:Float):Void

**program paths**:

* [ ] executablePath():String
* [ ] programPath():String

**other**:

* [x] args(): Array<String>;
* [ ] exit(code:Int):Void
* [ ] setTimeLocale(loc:String):Bool
* [ ] systemName():String

### FileSystem and File functions

`proxsys.Files` interface groups and abstracts methods defined in `sys.FileSystem` and `sys.io.Files`.

Currently supported functions:

#### FileSystem functions

* [x] absolutePath(relPath:String):String
* [ ] createDirectory(path:String):Void
* [ ] deleteDirectory(path:String):Void
* [ ] deleteFile(path:String):Void
* [x] exists(path:String):Bool
* [ ] fullPath(relPath:String):String
* [x] isDirectory(path:String):Bool
* [x] readDirectory(path:String):Array<String>
* [ ] rename(path:String, newPath:String):Void
* [x] stat(path:String):FileStat

#### File functions

* [x] getBytes(path:String):Bytes
* [x] getContent(path:String):String
* [x] saveBytes(path:String, bytes:Bytes):Void
* [x] saveContent(path:String, content:String):Void

* [ ] copy(srcPath:String, dstPath:String):Void

* [ ] append(path:String, binary:Bool = true):FileOutput
* [ ] read(path:String, binary:Bool = true):FileInput
* [ ] update(path:String, binary:Bool = true):FileOutput
* [ ] write(path:String, binary:Bool = true):FileOutput


------------------------------------------------------------------------

## Supported systems

Currently testing on `neko` and `nodejs`, but should work with other `sys` languages.

## Requirements

For `nodejs` requires `deasync` package.

Install it on your project with:

`npm install --save deasync`

Or:

`yarnpkg add deasync`

## How to Use

Currently, the main interface of the library is `System`, which abstracts system (Sys) methods.
You can use this interface on your code to allow switch for different implementations.

For example, consider this component class:

```haxe

class Component{
    //...

    public function new(sys: System){
        this.sys = sys;
        //...
    }
    //...
    public function executeCommand(){
        this.sys.command("example-command", ["arg1", "arg2"]);
    }
}
```

In your application code, you can instantiate this class passing the `StdSystem` class,
which redirects methods to the haxe `Sys`.

```haxe
public function main(){
    var component = new Component(new StdSystem());
    //...
    component.executeCommand();
}
```

However in test methods, you may use `FakeSystem` to fake the underlying system responses.
For example:

```haxe
public function myTest(){
    var sys = new FakeSystem();

    var component = new Component(sys);

    component.executeCommand();

    Assert.areEqual("example-command arg1 arg2", sys.lastCommand());
}
```

--------------

## Contributors

### How to compile and test 

This project uses the [lix package manager](https://github.com/lix-pm/lix.client).
So, to install its dependencies run:

`lix download`

To run the tests, you can use:

`lix Run test <test_type> -<target>`

For example:

`lix Run test unit -neko`

`lix Run test integration -js`

--------------

## License

This project is licensed under the MIT [non-AI license](https://github.com/non-ai-licenses/non-ai-licenses). See [LICENSE.md](./LICENSE.md) file.

So the content is this repository is not to be used on AI training datasets or technologies.