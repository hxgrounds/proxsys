//! -lib tink_cli -cp src

package;


import tink.Cli;
import tink.cli.Rest;

import haxe.io.Path;

import proxsys.System;
import proxsys.StdSystem;

using proxsys.SystemTools;


class Run{
    public static function main(){
	    Cli.process(Sys.args(), new Run()).handle(Cli.exit);
    }

    var sys:System;
    
    public function new(){
        sys = new StdSystem();
    }

    // ------------------------------------------------------------------

    @:command('test')
    public function test(testType: String, ?platform: String) {
        var cwd = sys.getCwd();

        var exitCode = 0;

        sys.withCwd(cwd + 'tests/$testType', ()->{
            exitCode = runTest(platform);
        });

        Sys.exit(exitCode);
    }

    function runTest(?platform:String) {
        var args = ['run', 'munit', 'test'];
        if(platform != null){
            args.push(platform);
        }

        return Sys.command('lix', args);
    }

    // ------------------------------------------------------------------

    @:defaultCommand
    public function help(args:Rest<String>) {
        Sys.println(usageHeader() + Cli.getDoc(this));
    }

    function usageHeader(): String{
        return 'Usage: ${programName()} [flags]\n';
    }

    function programName(): String {
        var programFilename = Path.withoutDirectory(Sys.programPath());

        return programFilename;
    }
}