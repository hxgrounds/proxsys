#!/bin/sh

mkdir -p build
rm -f build/library.zip
zip -r build/library.zip src *.md *.json

# Password should be stored as base64
export DECODED_PASS=$(echo $HAXELIB_PASS | base64 --decode)

haxelib submit build/library.zip "$DECODED_PASS" --always