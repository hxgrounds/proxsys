package proxsys;

import haxe.Constraints.IMap;
import proxsys.fakes.FakeFiles;
import proxsys.fakes.command.CommandSpecMatcher;
import proxsys.fakes.CommandMatcher;
import proxsys.fakes.FakeProcess;


class FakeSystem implements System{

    var lastCmd:String;
    var env:Map<String,String>;
    var stdoutText:String;

    var commandMatcher:CommandMatcher<FakeProcess>;
    var executedCommands:CommandMatcher<FakeProcess>;

    var defaultProc:FakeProcess;

    var fakeFiles:FakeFiles;

    public function new() {
        env = new Map();
        stdoutText = '';

        commandMatcher = new CommandMatcher(CommandMatcher.matchBaseCommand);
        executedCommands = new CommandMatcher((stored, query)->{
            return if(query.args == null) stored.cmd == query.cmd
                    else query.exactMatch(stored);
        });

        defaultProc = new FakeProcess();
        fakeFiles = new FakeFiles();
    }

    public function environment(): IMap<String, String> {
        return env.copy();
    }

    public function putEnvironment(envs:Map<String, String>) {
        for(key => value in envs){
            putEnv(key, value);
        }
    }

    public function files():Files {
        return fakeFiles;
    }


    public function command(cmd:String, ?args:Array<String>):Int{
        return startProcess(cmd, args).exitCode();
    }

    @:deprecated
    public function lastCommand(): String {
        return lastCmd;
    }

    public function args(): Array<String>{
        return [];
    }

    public function getEnv(s:String):String{
        return env.get(s);
    }
    public function putEnv(s:String, v:Null<String>):Void{
        env.set(s, v);
    }

    public function printedText(): String {
        return stdoutText;
    }

    public function print(s:String) {
        stdoutText += s;
    }

    public function println(s:String) {
        print('$s\n');
    }

    public function getCwd():String {
        return this.fakeFiles.getCwd();
    }

    public function setCwd(newDir:String) {
        this.fakeFiles.setCwd(newDir);
    }

    public function givenProcess(commandMatch:String, ?args: Array<String>) {
        var p = cloneDefaultProcess();
        this.commandMatcher.push(p, commandMatch, args);

        return p;
    }

    public function startProcess(cmd:String, ?args:Array<String>):ProcessInstance {
        var proc = this.commandMatcher.pop(cmd, args, cloneDefaultProcess());
        this.executedCommands.push(proc, cmd, args);

        lastCmd = cmd + ((args == null || args.length == 0) ? '' : ' ' + args.join(' '));

        proc.execute(cmd, args);

        return proc;
    }

    public function getExecuted(
        cmd:String, ?args: Array<String>,
        ?matcher:CommandSpecMatcher, ?remove:Bool): FakeProcess
    {
        return if(remove != true)
                executedCommands.get(cmd, args, matcher)
            else executedCommands.pop(cmd, args, matcher);
    }

    public function defaultProcess(): FakeProcess {
        return defaultProc;
    }

    function cloneDefaultProcess() {
        return this.defaultProc.clone();
    }

    public function popExecuted(cmd:String, ?args:Array<String>, ?matcher:CommandSpecMatcher) {
        return getExecuted(cmd, args, matcher, true);
    }
}