package proxsys;

import sys.FileStat;
import haxe.io.Bytes;

enum RemoveMode {
    Single;
    Recursive;
}

interface Files {
    function exists(path: String): Bool;
    function absolutePath(relPath:String): String;
    function isDirectory(path:String): Bool;
    function readDirectory(path:String): Array<String>;

    function getContent(path:String):String;
    function saveContent(path:String, content:String):Void;

    function saveBytes(path:String, content:Bytes):Void;
    function getBytes(path:String):Bytes;

    function stat(path:String):FileStat;

    function rename(path:String, newPath:String): Void;

    /**
        Removes a file or directory path (with or without recursion).

        Throws {@link proxsys.exceptions.FailedToRemoveException} when cannot remove file or directory.

        @return false if no file or directory was removed, and true otherwise.
    **/
    function remove(path:String, mode:RemoveMode=RemoveMode.Single):Bool;
}