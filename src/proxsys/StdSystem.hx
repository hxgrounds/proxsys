package proxsys;

import haxe.Constraints.IMap;
import proxsys.std.StdFiles;
import proxsys.std.StdProcess;

class StdSystem implements System{
    var _files:Files;

    public function new() {
        this._files = new StdFiles();
    }


    public function command(cmd:String, ?args:Array<String>):Int{
        #if debug
        trace('command: $cmd $args');
        #end

        return Sys.command(cmd, args);
    }

    public function args(): Array<String>{
        return Sys.args();
    }

    public function getEnv(s:String):String{
        return Sys.getEnv(s);
    }
    public function putEnv(s:String, v:Null<String>):Void{
        Sys.putEnv(s, v);
    }

    public function print(s:String) {Sys.print(s);}
    public function println(s:String) {Sys.println(s);}

    public function getCwd():String { return Sys.getCwd(); }

    public function setCwd(newDir:String) {
        Sys.setCwd(newDir);
    }

    public function startProcess(cmd:String, ?args:Array<String>):ProcessInstance {
        return new StdProcess(cmd, args);
    }

    public function files():Files {
        return _files;
    }

    public function environment():IMap<String, String> {
        return Sys.environment();
    }
}