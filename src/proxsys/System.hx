package proxsys;

import haxe.Constraints.IMap;

interface System {
    function command(cmd:String, ?args:Array<String>):Int;
    function args(): Array<String>;


    function getEnv(s:String):String;
    function putEnv(s:String, v:Null<String>):Void;
    function environment():IMap<String, String>;

    function print(s:String):Void;
    function println(s:String):Void;

    function getCwd():String;
    function setCwd(newDir:String):Void;

    function startProcess(cmd:String, ?args:Array<String>):ProcessInstance;

    function files():Files;
}