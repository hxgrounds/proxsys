package proxsys;

class SystemTools {
    public static function withCwd(sys:System, newDir:String, func:()->Void) {
        var previousDir = sys.getCwd();

        try {
            sys.setCwd(newDir);

            func();
        }
        catch(e){
            sys.setCwd(previousDir);

            throw e;
        }

        sys.setCwd(previousDir);
    }
}