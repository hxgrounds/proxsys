package proxsys.exceptions;

import haxe.Exception;

class PathNotFoundException extends FileSystemException
{
    @:optional
    public var path(default, null):String;

    public function new(path:String, ?message:String, ?previous:Exception, ?native:Any) {
        super(buildMessage(path, message), previous, native);

        this.path = path;
    }

    static function buildMessage(path:String, ?message:String):String {
        if(message != null)
            return message;

        return 'Path not found: $path';
    }
}