package proxsys.fakes;

import proxsys.fakes.command.CommandSpecMatcher;
import proxsys.fakes.command.CommandInstance;
import proxsys.fakes.command.CommandSpec;


typedef CustomMatcher = CommandSpecMatcher;

class CommandMatcher<T> {

    public static function matchBaseCommand(stored:CommandSpec, matching:CommandSpec){
        return stored.cmd == matching.cmd
            && !stored.hasArgs();
    }

    public static function exactMatcher(stored:CommandSpec, matching:CommandSpec){
        return stored.exactMatch(matching);
    }

    var cmdToInstances:Map<String, List<CommandInstance<T>>>;
    var defaultMatcher:Null<CommandSpecMatcher>;

    public function new(?defaultMatcher: CommandSpecMatcher) {
        this.cmdToInstances = new Map();
        this.defaultMatcher = defaultMatcher;
    }

    public function get(
        cmd: String, ?args: Array<String>, ?matcher:CustomMatcher, ?defaultProc: T): Null<T>
    {
        return retrieve(new CommandSpec(cmd, args), matcher, defaultProc);
    }

    public function pop(
        cmd: String, ?args: Array<String>, ?matcher:CustomMatcher, ?defaultProc: T): Null<T>
    {
        return retrieve(new CommandSpec(cmd, args), matcher, defaultProc, true);
    }

    function retrieve(
        spec:CommandSpec, ?matcher:CustomMatcher, ?defaultProc:T, remove:Bool=false): Null<T>
    {
        var cmdInstances = cmdToInstances.get(spec.cmd);
        if(cmdInstances == null)
            return defaultProc;

        var proc = null;
        var matchArgs = if(matcher != null && spec.args == null)
                            matcher
                        else exactMatcher;

        for(m in [matchArgs, defaultMatcher]){
            if(m == null) continue;

            proc = selectProcess(cmdInstances, spec, remove, (stored, query)->{
                return m(stored, query)
                    && (matcher == null || matcher(stored, query));
            });

            if(proc != null){
                return proc;
            }
        }

        return defaultProc;
    }

    public function push(proc:T, cmd: String, ?args: Array<String>) {
        var cmdInstances = cmdToInstances.get(cmd);

        if(cmdInstances == null){
            cmdInstances = new List();
            cmdToInstances.set(cmd, cmdInstances);
        }

        cmdInstances.add(new CommandInstance(proc, cmd, args));
    }

    // Helpers ----------------------------------------------------------

    function selectProcess(
        commands:List<CommandInstance<T>>, spec:CommandSpec, remove:Bool, matcher:CustomMatcher)
    {
        if(commands == null || commands.isEmpty()){
            return null;
        }

        for(cmdInstance in commands){
            if(matcher(cmdInstance, spec)){
                if(remove){
                    commands.remove(cmdInstance);
                }

                return cmdInstance.instance;
            }
        }

        return null;
    }
}