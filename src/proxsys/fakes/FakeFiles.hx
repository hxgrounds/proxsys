package proxsys.fakes;

import proxsys.Files.RemoveMode;
import proxsys.exceptions.FailedToRemoveException;
import proxsys.exceptions.PathNotFoundException;
import proxsys.fakes.files.FileStatBuilder;
import sys.FileStat;
import haxe.io.Bytes;
import haxe.ds.BalancedTree;
import haxe.Constraints.IMap;
import haxe.io.Path;

using StringTools;


enum FileTypes {
    FileType;
    DirectoryType;
}

@:structInit
class FileInfo {
    @:optional
    public var type:Null<FileTypes> = null;

    @:optional
    public var content:Null<Bytes> = null;

    @:optional
    public var stat:FileStat;

    @:optional
    public var children:IMap<String, FileInfo> = new Map<String, FileInfo>();

    public function addChild(name:String, info:FileInfo) {
        children.set(name, info);
    }

    public function removeChild(name:String) {
        children.remove(name);
    }

    public function listChildren(): Iterator<String>{
        return children.keys();
    }

    public function hasChildren() {
        return children.keys().hasNext();
    }

    public function clone(): FileInfo{
        return {
            type: type,
            stat: Reflect.copy(stat),
            content: cloneContent(),
            children: children.copy()
        };
    }

    function cloneContent():Bytes {
        if(content == null)
            return null;
        var newBytes = Bytes.alloc(content.length);
        newBytes.blit(0, content, 0, content.length);

        return newBytes;
    }
}

class FakeFiles implements Files{
    var cwd:String = '/';
    var fTree:FilesTree;

    public function new() {
        this.fTree = new FilesTree((path)->absoluteNormalPath(path));
    }

    public function getCwd(): String{
        return cwd;
    }

    public function setCwd(path:String) {
       this.cwd = absolutePath(path);
    }

    public function absolutePath(relPath:String): String {
        var p = Path.removeTrailingSlashes(relPath);

        if(Path.isAbsolute(p)){
            return p;
        }

        return Path.join([cwd, p]);
    }

    function absoluteNormalPath(relPath:String): String {
        return Path.normalize(absolutePath(relPath));
    }

    public function exists(path:String): Bool {
        return fTree.exists(path);
    }

    public function putPath(path:String, fileType:FileTypes=FileType) {
        fTree.putPath(path, fileType);
    }

    public function rename(srcPath:String, dstPath:String) {
        if(!exists(srcPath)){
            throw new PathNotFoundException(srcPath);
        }

        if(fTree.areSamePath(srcPath, dstPath)){
            return;
        }

        fTree.copyPath(srcPath, dstPath);
        fTree.removePath(srcPath, true);
    }


    public function isDirectory(path:String): Bool {
        var fileInfo = fTree.getFileInfo(path);

        return fileInfo != null && fileInfo.type == DirectoryType;
    }

    public function readDirectory(path:String): Array<String> {
        var fileInfo = fTree.getFileInfo(path);

        return [
            for(child in fileInfo.listChildren())
                child
        ];
    }

    public function remove(path:String, mode:RemoveMode=RemoveMode.Single): Bool {
        var isRecursive = (mode == RemoveMode.Recursive);

        return fTree.removePath(path, isRecursive);
    }

    public function removePath(path:String) {
        remove(path, RemoveMode.Recursive);
    }

    public function getBytes(path:String): Bytes {
        var fileInfo = fTree.getFileInfo(path);

        return if(fileInfo != null) fileInfo.content else null;
    }

    public function saveBytes(path:String, bytes:Bytes) {
        fTree.putPath(path, FileType);
        fTree.getFileInfo(path).content = bytes;
    }

    public function getContent(path:String):String {
        var bytes = getBytes(path);

        return if(bytes == null) null else bytes.toString();
    }

    public function saveContent(path:String, content:String) {
        this.saveBytes(path, Bytes.ofString(content));
    }

    public function stat(path:String): FileStat {
        return fTree.getFileInfo(path).stat;
    }

    public function setStat(path:String, fileStat:FileStat) {
        var fileInfo = fTree.getFileInfo(path);

        fileInfo.stat = fileStat;
    }
}

class FilesTree{
    var normalizePath:(String)->String;
    var existingFiles:IMap<String, FileInfo>;

    public function new(normalizePath:(String)->String) {
        this.normalizePath = normalizePath;
        this.existingFiles = new BalancedTree<String, FileInfo>();
    }

    function absoluteNormalPath(relPath:String): String {
        return normalizePath(relPath);
    }

    public function exists(path:String): Bool {
        var p = absoluteNormalPath(path);

        return existingFiles.exists(p);
    }

    public function putPath(path:String, fileType:FileTypes=FileType) {
        var p = absoluteNormalPath(path);

        var segments = p.split("/");
        var parent:FileInfo = null;
        for (i in 0...segments.length - 1){
            var subpath = segments.slice(0, i + 1).join("/");

            // TO-DO: require subpath is directory (cannot create path under file)
            parent = addChildFile(subpath, DirectoryType, parent);
        }
        addChildFile(p, fileType, parent);
    }

    function addChildFile(path:String, fileType:FileTypes, parent:FileInfo=null) {
        var info = getOrMakeFileInfo(path, fileType);

        if(parent != null){
            var child = Path.withoutDirectory(path);

            parent.addChild(child, info);
        }

        return info;
    }

    function getOrMakeFileInfo(path:String, fileType:FileTypes) {
        var info = getFileInfo(path);
        if(info == null){
            info = {
                type:fileType,
                stat:FileStatBuilder.defaultStat()
            };
            setFileInfo(path, info);
        }

        return info;
    }

    public function getFileInfo(path:String) {
        return existingFiles.get(absoluteNormalPath(path));
    }
    public function setFileInfo(path:String, info:FileInfo) {
        existingFiles.set(absoluteNormalPath(path), info);
    }

    public function copyPath(srcPath:String, dstPath:String) {
        var srcInfo = getFileInfo(srcPath);
        return copySubtree(srcInfo, srcPath, dstPath);
    }

    function copySubtree(srcInfo:FileInfo, srcPath:String, dstPath:String) {
        var dstInfo  = srcInfo.clone();

        for (child=>info in srcInfo.children.keyValueIterator()){
            var childInfo = copySubtree(info,
                Path.join([srcPath, child]),
                Path.join([dstPath, child])
            );

            dstInfo.addChild(child, childInfo);
        }

        setFileInfo(dstPath, dstInfo);

        return dstInfo;
    }


    public function removePath(path:String,recursive:Bool=false): Bool{
        var p = absoluteNormalPath(path);
        var info = getFileInfo(p);

        if(info == null){
            return false;
        }

        if(recursive){
            for (child in info.listChildren()){
                var childPath = absoluteNormalPath(Path.join([p, child]));

                removePath(childPath, recursive);
            }
        }
        else if(info.hasChildren()){
            // Cannot remove directory
            throw new FailedToRemoveException(
                'Cannot remove non empty directory $p without recursion enabled.');
        }

        if(existingFiles.remove(p)){
            removeFromParent(p);
            return true;
        }
        return false;
    }

    function removeFromParent(path:String) {
        var parentPath = Path.directory(absoluteNormalPath(path));
        var parentInfo = getFileInfo(parentPath);

        if(parentInfo != null){
            parentInfo.removeChild(Path.withoutDirectory(path));
        }
    }

    public function areSamePath(pathA:String, pathB:String) {
        return absoluteNormalPath(pathA) == absoluteNormalPath(pathB);
    }
}