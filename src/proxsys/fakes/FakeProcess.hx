package proxsys.fakes;

import haxe.io.Bytes;
import haxe.io.BytesOutput;
import haxe.io.StringInput;
import haxe.io.Output;
import haxe.io.Input;

typedef CommandListener = (cmd:String, args:Array<String>)->Void;

class FakeProcess implements ProcessInstance{
    public var stdout(default, null):Input;
    public var stdin(default, null):Output;

    var outputText:String;

    public var wasKilled(default, null):Bool = false;

    static var pid_counter = 1;
    var pid:Int;
    var exit_code:Null<Int>;

    var cmdListener:CommandListener = null;

    public function new() {
        outputText = '';
        stdout = new StringInput('');
        stdin = new BytesOutput();

        pid = pid_counter++;
    }

    public function kill() {
        wasKilled = true;
    }

    public function setExitCode(exitCode:Int) {
        this.exit_code = exitCode;
        return this;
    }

    public function exitCode(block:Bool = true):Null<Int> {
        if(block && this.exit_code == null){
            this.exit_code = 0;
        }

        return this.exit_code;
    }

    public function onCall(executor:CommandListener) {
        this.cmdListener = executor;

        return this;
    }

    public function getPid():Int {
        return pid;
    }


    /** Sets the expected output **/
    public function output(s:String) {
        this.outputText = s;
        this.stdout = new StringInput(this.outputText);
        return this;
    }

    public function getOutputText() {
        return outputText;
    }

    public function clone(): FakeProcess{
        var proc = new FakeProcess().setExitCode(exit_code).output(outputText);

        var bytes = writtenBytes();
        proc.stdin.writeBytes(bytes, 0, bytes.length);
        proc.wasKilled = this.wasKilled;

        return proc;
    }

    public function execute(cmd:String, args:Array<String>) {
        if(this.cmdListener != null){
            this.cmdListener(cmd, args);
        }
    }

    public function writtenBytes(): Bytes {
        var bytes = cast(stdin, BytesOutput).getBytes();
        this.stdin = new BytesOutput();
        this.stdin.writeBytes(bytes, 0, bytes.length);

        return bytes;
    }
}