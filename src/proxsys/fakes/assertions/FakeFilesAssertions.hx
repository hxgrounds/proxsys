package proxsys.fakes.assertions;

import haxe.PosInfos;
import org.hamcrest.Matchers.*;

class FakeFilesAssertions {
    public static
    function pathShouldExist(fakeFiles:FakeFiles, checkPath:String, exists:Bool=true, ?pos:PosInfos)
    {
        var should = shouldText(exists);

        assertThat(fakeFiles.exists(checkPath), is(exists),
            'Path $checkPath $should exist', pos);
    }

    public static
    function shouldBeADirectory(fakeFiles:FakeFiles, path:String, shouldBe:Bool=true, ?pos:PosInfos) {
        var should = shouldText(shouldBe);

        assertThat(fakeFiles.isDirectory(path), is(shouldBe),
            '$path $should be a directory', pos);
    }

    static function shouldText(should:Bool) {
        return should ? 'should' : 'should not';
    }
}