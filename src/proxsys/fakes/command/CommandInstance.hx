package proxsys.fakes.command;

@:structInit
class CommandInstance<T> extends CommandSpec{
    public var instance:Null<T>;

    public function new(instance:Null<T>, cmd:String, ?args:Array<String>) {
        super(cmd, args);
        this.instance = instance;
    }
}