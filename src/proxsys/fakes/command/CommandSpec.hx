package proxsys.fakes.command;


@:structInit
class CommandSpec {
    public var cmd: String;
    public var args: Array<String>;

    public function new(cmd:String, ?args:Array<String>) {
        this.cmd = cmd;
        this.args = args;
    }

    public function hasArgs() {
        return this.args != null && this.args.length > 0;
    }

    public function match(cmd:String, ?args:Array<String>): Bool {
        return this.cmd == cmd
            && (this.args == null
                || (argsAreEmpty(this.args) && argsAreEmpty(args))
                || areEquals(this.args, args));
    }

    public function exactMatch(query:CommandSpec) {
        return this.cmd == query.cmd
            && ((argsAreEmpty(this.args) && argsAreEmpty(query.args))
                || areEquals(this.args, query.args));
    }

    public static function argsAreEmpty(args:Array<String>) {
        return args == null || args.length == 0;
    }

    function areEquals(arr1:Array<String>, arr2:Null<Array<String>>) {
        if(arr1 == null || arr2 == null){
            return arr1 == arr2;
        }
        if(arr1 == arr2) return true;
        if(arr1.length != arr2.length) return false;

        for (i in 0...arr1.length){
            if(arr1[i] != arr2[i]){
                return false;
            }
        }

        return true;
    }

    public function containsArgs(args:Array<String>) {
        if(!hasArgs())
            return args == null || args.length == 0;

        for(arg in args){
            if(!containArg(arg)){
                return false;
            }
        }

        return true;
    }

    public function containArg(arg:String) {
        return hasArgs() && this.args.contains(arg);
    }
}