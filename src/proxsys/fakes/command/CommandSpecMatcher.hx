package proxsys.fakes.command;

import proxsys.fakes.command.MatchCommandsSpec;


@:callable
abstract CommandSpecMatcher(MatchCommandsSpec)
    from MatchCommandsSpec
    to MatchCommandsSpec
{
    public function new(matcher: MatchCommandsSpec) {
        this = matcher;
    }

    @:from
    public static function fromSimpleMatcher(matcher: (CommandSpec)->Bool) {
        return new CommandSpecMatcher((stored, query)->matcher(stored));
    }
}