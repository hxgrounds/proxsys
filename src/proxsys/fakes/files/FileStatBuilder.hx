package proxsys.fakes.files;

import sys.FileStat;


class FileStatBuilder {
    var stat:FileStat;

    public function new() {
        this.stat = defaultStat();
    }

    public static function builder() {
        return new FileStatBuilder();
    }

    public static function defaultStat() {
        return {
            gid: 0,
            uid: 0,
            mode: buildMode(2,2,2),
            rdev: 0,
            nlink: 0,
            ino: 0,
            dev: 0,
            size: 0,
            ctime: Date.now(),
            mtime: Date.now(),
            atime: Date.now()
        };
    }

    public function gid(gid: Int) {
        this.stat.gid = gid;
        return this;
    }
    public function uid(uid: Int) {
        this.stat.uid = uid;
        return this;
    }
    public function mode(mode: Int) {
        this.stat.mode = mode;
        return this;
    }
    public function modeParts(user:Int, group:Int, others:Int) {
        return mode(buildMode(user, group, others));
    }
    public function dev(value: Int) {
        this.stat.rdev = value;
        return this;
    }
    public function rdev(value: Int) {
        this.stat.dev = value;
        return this;
    }
    public function ino(value: Int) {
        this.stat.ino = value;
        return this;
    }
    public function nlink(value: Int) {
        this.stat.nlink = value;
        return this;
    }
    public function size(value: Int) {
        this.stat.size = value;
        return this;
    }
    public function ctime(date: Date) {
        this.stat.ctime = date;
        return this;
    }
    public function mtime(date: Date) {
        this.stat.mtime = date;
        return this;
    }
    public function atime(date: Date) {
        this.stat.atime = date;
        return this;
    }

    public function build() {
        return Reflect.copy(this.stat);
    }

    static function buildMode(user:Int, group:Int, others:Int) {
        return user << 6 | group << 3 | others;
    }
}