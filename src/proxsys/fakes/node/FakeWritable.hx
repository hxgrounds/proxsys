package proxsys.fakes.node;

import js.Lib;
import js.node.Buffer;
import haxe.io.Bytes;
#if (nodejs)

import haxe.extern.EitherType;

import js.lib.Error;
import js.node.Stream;
import js.node.stream.Writable.IWritable;

class FakeWritable extends Stream<FakeWritable> implements IWritable {

    public var writable(default, null):Bool = true;
    public var writableEnded(default, null):Bool = false;
    public var writableFinished(default, null):Bool = false;
    public var writablehighWaterMark(default, null):Int = 16000;
    public var writableLength(default, null):Int = 0;
    public var writableObjectMode(default, null):Bool = false;
    public var isTTY(default, null):Bool = false;
    public var destroyed(default, null):Bool = false;

    public var writedDataHex(default, null):String = '';

    var nextWriteReturn = true;

    public function nextWriteShouldReturn(value:Bool) {
        this.nextWriteReturn = value;
    }


    public function setDefaultEncoding(encoding:String):IWritable
    { return this;}

    public function cork():Void{}

    public function uncork():Void
    {}

    /**
        @param chunk: <string> | <Buffer> | <Uint8Array> | <any> - Optional data to write.
        For streams not operating in object mode, chunk must be a string, Buffer or Uint8Array.
        For object mode streams, chunk may be any JavaScript value other than null.
    **/
    public function write(chunk:Dynamic, ?encoding:String, ?callback:EitherType<Void->Void, Null<Error>->Void>):Bool
    {
        var bytes = getBytesFrom(chunk);

        writedDataHex += bytes.toHex();

        return nextWriteReturn;
    }

    function getBytesFrom(chunk:Dynamic): Bytes {
        if(Buffer.isBuffer(chunk)){
            return cast(chunk, Buffer).hxToBytes();
        }

        return Buffer.from(chunk).hxToBytes();
    }

    public function destroy(?error:Error):IWritable
    {
        return this;
    }


    @:overload(function(?callback:EitherType<Void->Void, Null<Error>->Void>):Void {})
    public function end(chunk:Dynamic, ?encoding:String, ?callback:EitherType<Void->Void, Null<Error>->Void>):Void
    {

    }
}

#end