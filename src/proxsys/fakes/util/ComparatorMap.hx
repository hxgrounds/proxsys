package proxsys.fakes.util;

import haxe.ds.BalancedTree;

typedef Comparator<K> = (K, K)->Int;

class ComparatorMap<K, V> extends BalancedTree<K,V> {
    var comparator:Comparator<K>;

    public function new(comparator:Comparator<K>) {
        super();
        this.comparator = comparator;
    }

    override function compare(k1:K, k2:K):Int {
        return this.comparator(k1, k2);
    }
}