package proxsys.fakes.util;

import proxsys.util.Wait;

class FakeWait implements Wait {

    public var lastWaitCondition: ()->Bool = null;

    public function new() {
    }

    public function waitUntil(condition:() -> Bool) {
        lastWaitCondition = condition;
    }
}