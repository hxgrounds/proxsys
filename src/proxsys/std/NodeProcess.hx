package proxsys.std;

import sys.NodeSync;
import proxsys.std.node.WritableOutput;
import js.node.ChildProcess;
import haxe.io.Output;
import haxe.io.Input;

import proxsys.std.node.ReadableInput;


typedef ChildProcessObject = js.node.child_process.ChildProcess;

class NodeProcess implements ProcessInstance{
	public var stdout(default, null):Input;

	public var stdin(default, null):Output;

    var proc:ChildProcessObject;
    var exit_code:Null<Int>;

    public function new(cmd: String, ?args: Array<String>) {
        this.proc = ChildProcess.spawn(cmd, args);
        this.stdout = new ReadableInput(proc.stdout);
        this.stdin  = new WritableOutput(proc.stdin);
        this.exit_code = null;
        this.proc.once('exit', this.onExit);
    }

    public function kill() {
        this.proc.kill();
    }

    function signalToCode(signal:String): Null<Int>{
        if(signal == null) return null;

        var code = [
            'SIGHUP'    => 1, 'SIGINT'    => 2, 'SIGQUIT' =>  3,  'SIGILL'  => 4, 
            'SIGTRAP'   => 5, 'SIGABRT'   => 6, 'SIGIOT'  =>  6,  'SIGBUS'  => 7,
            'SIGEMT'    => 7, 'SIGFPE'    => 8, 'SIGKILL' =>  9,  'SIGUSR1' => 10,
            'SIGSEGV'   => 11,'SIGUSR2'   => 12,'SIGPIPE' =>  13, 'SIGALRM' => 14,
            'SIGTERM'   => 15,'SIGSTKFLT' => 16,'SIGCHLD' =>  17, 'SIGCLD'  => 18,
            'SIGCONT'   => 18,'SIGSTOP'   => 19,'SIGTSTP' =>  20, 'SIGTTIN' => 21,
            'SIGTTOU'   => 22,'SIGURG'    => 23,'SIGXCPU' =>  24, 'SIGXFSZ' => 25,
            'SIGVTALRM' => 26,'SIGPROF'   => 27,'SIGWINCH'=>  28, 'SIGIO'   => 29,
            'SIGPOLL'   => 29,'SIGPWR'    => 30,'SIGINFO' =>  29, 'SIGLOST' => 29,
            'SIGSYS'    => 31,'SIGUNUSED' => 31
        ].get(signal);

        return if(code != null) code else -1;
    }

    function onExit(code: Null<Int>, signal: String) {
        this.exit_code = if(code != null) code else signalToCode(signal);
    }

    public function exitCode(block:Bool = true):Null<Int> {
        if(block && !hasFinished()){
            NodeSync.wait(hasFinished);
        }

        return this.exit_code;
    }

    function hasFinished() {
        return this.exit_code != null;
    }

    public function getPid():Int {
        return this.proc.pid;
    }
}