package proxsys.std;

import proxsys.Files.RemoveMode;
import proxsys.exceptions.FailedToRemoveException;
import haxe.io.Path;
import proxsys.exceptions.PathNotFoundException;
import sys.FileStat;
import haxe.io.Bytes;
import sys.io.File;
import sys.FileSystem;

class StdFiles implements Files{
    public function new() {}

    public function exists(path:String):Bool {
        return FileSystem.exists(path);
    }

    public function absolutePath(relPath:String):String {
        return FileSystem.absolutePath(relPath);
    }

    public function isDirectory(path:String):Bool {
        return FileSystem.isDirectory(path);
    }

    public function readDirectory(path:String):Array<String> {
        return FileSystem.readDirectory(path);
    }

    public function getContent(path:String):String {
        return File.getContent(path);
    }

    public function saveContent(path:String, content:String) {
        File.saveContent(path, content);
    }

    public function saveBytes(path:String, content:Bytes) {
        File.saveBytes(path, content);
    }

    public function getBytes(path:String):Bytes {
        return File.getBytes(path);
    }

    public function stat(path:String):FileStat {
        return FileSystem.stat(path);
    }

    public function rename(path:String, newPath:String) {
        if(!exists(path))
            throw new PathNotFoundException(path);

        FileSystem.rename(path, newPath);
    }

    public function remove(path:String, mode:RemoveMode=RemoveMode.Single):Bool {
        if(!exists(path))
            return false;

        if(!FileSystem.isDirectory(path)){
            FileSystem.deleteFile(path);
        }
        else{
            removeDirectory(path, mode);
        }

        return !exists(path);
    }

    function removeDirectory(path:String, mode:RemoveMode) {
        var recursive = (mode == RemoveMode.Recursive);
        var children  = readDirectory(path);

        if(!recursive && children.length > 0){
            throw new FailedToRemoveException(
                'Cannot remove directory with children in non recursive mode'
                + ' (path: $path)'
            );
        }

        if(recursive){
            for(f in children){
                var subfile = Path.join([path, f]);
                remove(subfile, mode);
            }
        }

        try {
            FileSystem.deleteDirectory(path);
        }
        catch(e){
            throw new FailedToRemoveException(
                'Could not remove directory $path', e);
        }
    }
}