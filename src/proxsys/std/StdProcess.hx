package proxsys.std;

#if sys
import proxsys.std.SysProcess;

typedef StdProcess = SysProcess;

#elseif nodejs

typedef StdProcess = proxsys.std.NodeProcess;

#end