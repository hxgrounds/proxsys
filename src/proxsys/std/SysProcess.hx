package proxsys.std;

#if sys
import sys.io.Process;

class SysProcess extends Process implements ProcessInstance {
    static final KILLED_EXIT_CODE = -1000;

    public function new(cmd: String, ?args: Array<String>) {
        super(cmd, args);
    }

    #if neko
    override public function exitCode(block:Bool = true):Null<Int> {
        try {
            return super.exitCode(block);
        }
        catch(e){
            if(e.message.indexOf('killed') >= 0){
                return KILLED_EXIT_CODE;
            }

            throw e;
        }
    }
    #end
}
#else
class SysProcess {}
#end