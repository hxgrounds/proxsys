package proxsys.std.node;

#if nodejs
import sys.NodeSync;

class NodeSyncWait implements proxsys.util.Wait{
    public function new() {
    }

    public function waitUntil(condition:() -> Bool) {
        NodeSync.wait(condition);
    }
}

#end