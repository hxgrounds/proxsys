package proxsys.std.node;

import haxe.io.Bytes;
import haxe.Exception;
import js.Lib;
import sys.NodeSync;
import haxe.io.Eof;
import js.node.Buffer;
import js.node.stream.Readable.IReadable;
import haxe.io.Input;

class ReadableInput extends Input {
    var readable:IReadable;

    public function new(readable:IReadable) {
        this.readable = readable;
    }

    /**
        read one byte from Readable.

        If readable has ended, throw Eof.
        If no byte is available block until there is some data.
    **/
    public override function readByte():Int {
        var buf = readBuffer(1);

        return buf.readUInt8(0);
    }

    public override function readBytes(s:Bytes, pos:Int, len:Int):Int {
        var buf = readBuffer(len);
        var bytes = buf.hxToBytes();

        s.blit(pos, bytes, 0, buf.length);

        return buf.length;
    }

    function readBuffer(len: Int): Buffer {
        waitForBytes(1);

        var numBytes = this.readable.readableLength;

        if(ended() || numBytes == 0){
            throw new Eof();
        }

        var toRead = if(numBytes >= len) len else numBytes;
        var result = this.readable.read(toRead);

        if(Lib.typeof(result) == 'string'){
            result = Buffer.from(result);
        }
        if(!Buffer.isBuffer(result)){
            //TO-DO: use specific exception
            throw new Exception('Illegal value readed from Readable');
        }

        return result;
    }

    function waitForBytes(numBytes:Int) {
        var hasBytesOrEnded = ()->{
            return hasBytes(numBytes) || ended();
        }

        if(!hasBytesOrEnded()){
            NodeSync.wait(hasBytesOrEnded);
        }
    }

    function ended() {
        return this.readable.readableEnded || this.readable.destroyed;
    }

    function hasBytes(numBytes:Int) {
        return this.readable.readableLength >= numBytes;
    }
}