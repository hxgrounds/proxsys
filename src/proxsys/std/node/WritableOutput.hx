package proxsys.std.node;

import proxsys.util.Wait;
import haxe.io.Error;
import haxe.io.Bytes;
import haxe.io.Output;

import js.node.Buffer;
import js.node.stream.Writable.IWritable;


class WritableOutput extends Output{
    var writable:IWritable;
    var wait:Wait;
    var needDrain:Bool = false;


    public function new(writable:IWritable, ?wait:Wait) {
        this.writable = writable;
        this.wait = if(wait != null) wait else new NodeSyncWait();

        this.registerEvents();
    }

    function registerEvents() {
        this.writable.on('drain', onDrain);
    }

    function onDrain() {
        this.needDrain = false;
    }

    public override function close() {
        this.writable.end();
    }

    public override function writeByte(byte:Int) {
        writeBuffer(Buffer.from([byte]));
    }

    public override function writeBytes(s:Bytes, pos:Int, len:Int):Int {
        if (pos < 0 || len < 0 || pos + len > s.length)
            throw Error.OutsideBounds;

        var buf = Buffer.hxFromBytes(s);
        var bufSlice = buf.slice(pos, pos + len);

        return this.writeBuffer(bufSlice);
    }

    function writeBuffer(buf:js.node.buffer.Buffer, waitToWrite=true): Int {
        if(waitToWrite){
            this.waitAvailableSpace();
        }
        if(!this.writable.writable || !hasSpace()){
            return 0;
        }

        //NOTE: an error still can occur while writing, maybe we should wait for callback?
        this.needDrain = !this.writable.write(buf);

        return buf.length;
    }

    function waitAvailableSpace() {
        function hasSpaceOrEnded() {
            return (!needDrain && hasSpace()) || !this.writable.writable;
        }

        if(!hasSpaceOrEnded()){
            wait.waitUntil(hasSpaceOrEnded);
        }
    }

    function hasSpace() {
        final hightWaterMark = this.writable.writablehighWaterMark;

        return hightWaterMark == null
                || hightWaterMark > this.writable.writableLength;
    }
}