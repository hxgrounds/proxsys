package proxsys.tools;

import haxe.io.Eof;

class ProcessTools {

    public static function readAllText(process:ProcessInstance) {
        var allBytes = process.stdout.readAll();

        return if(allBytes == null || allBytes.length == 0) '' else allBytes.toString();
    }

    public static function readAllLines(process:ProcessInstance): Array<String> {
        var lines = [];

        try{
            var returnedNull = false;
            while(!returnedNull){
                var line = process.stdout.readLine();
                returnedNull = (line == null);

                if(!returnedNull){
                    lines.push(line);
                }
            }
        }
        catch(e: Eof)
        {}

        return lines;
    }
}