package proxsys.util;

interface Wait {
    /** Waits until condition function returns true. **/
    function waitUntil(condition: ()->Bool):Void;
}