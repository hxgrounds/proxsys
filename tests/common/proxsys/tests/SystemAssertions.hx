package proxsys.tests;

import haxe.Constraints.IMap;
import haxe.PosInfos;
import org.hamcrest.Matchers.*;

class SystemAssertions {
    static public function map_is_equal_to(
        env:IMap<String, String>, expected:IMap<String, String>, ?pos:PosInfos)
    {
        keys_should_be_equal_to(env, expected.keys(), pos);
        should_contain_values(env, expected, pos);
    }

    static public function environment_should_contain(
        system:System, env:haxe.ds.Map<String, String>, ?pos:PosInfos)
    {
        for(key => value in env){
            assertThat(system.getEnv(key), equalTo(value),
                'getEnv($key) should be $value', pos
            );
        }
    }

    public static function should_contain_values(
        env:IMap<String, String>, expected:IMap<String, String>, ?pos:Null<PosInfos>)
    {
        for(key => value in expected){
            assertThat(env.get(key), equalTo(value),
                '$key should point to $value', pos
            );
        }
    }

    static public
    function keys_should_be_equal_to(env:IMap<String, String>, expected:Iterator<String>, ?pos:PosInfos) {
        var keys = sorted(toArray(env.keys()));
        var expected_keys = sorted(toArray(expected));

        assertThat(keys, is(array(expected_keys)),
            'Keys does not match the expectation', pos);
    }

    static function toArray(items:Iterator<String>): Array<String> {
        return [for(item in items) item];
    }

    static function sorted<T>(items:Array<T>):Array<T> {
        var sortedItems = items.copy();
        sortedItems.sort(Reflect.compare);

        return sortedItems;
    }
}