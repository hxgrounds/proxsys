import massive.munit.TestSuite;

import proxsys.tests.StdSystemTest;
import proxsys.tests.std.StdProcessTest;
import proxsys.tests.std.StdFilesTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(proxsys.tests.StdSystemTest);
		add(proxsys.tests.std.StdProcessTest);
		add(proxsys.tests.std.StdFilesTest);
	}
}
