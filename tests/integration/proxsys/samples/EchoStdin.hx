package proxsys.samples;

import haxe.io.Eof;

class EchoStdin {
    public static function main() {
        new EchoStdin().run();
    }

    public function new() {}


    public function run() {
        try{
            var line:String = null;
            while ((line = Sys.stdin().readLine()) != null){
                Sys.println(line);
            }
        }
        catch(e: Eof){
            return;
        }
    }
}
