package proxsys.tests;

import haxe.io.Path;
import haxe.Exception;
import sys.FileSystem;
import org.hamcrest.Matchers.*;

import proxsys.std.StdProcess;
using proxsys.SystemTools;
using proxsys.tests.SystemAssertions;

class StdSystemTest {
    var sys:System;

    public function new() {}

    @Before
    public function setup() {
        sys = new StdSystem();
    }

    @Test
    public function get_cwd() {
        assertThat(sys.getCwd(), is(equalTo(Sys.getCwd())),
            'StdSystem.getCwd should match Sys.getCwd'
        );
    }

    @Test
    public function run_in_another_directory() {
        var cwd = Sys.getCwd();

        var newDir = FileSystem.fullPath(Path.join([cwd, '..']));
        var changedDir = null;

        sys.withCwd(newDir, ()->{
            changedDir = Sys.getCwd();
        });

        assertThat(FileSystem.fullPath(changedDir), is(equalTo(newDir)),
            'Should have changed directory inside function'
        );
        assertThat(Sys.getCwd(), is(equalTo(cwd)),
            'Should return to previous directory after finished function'
        );
    }

    @Test
    public function return_to_directory_even_with_failure() {
        var cwd = Sys.getCwd();

        try{
            sys.withCwd(Path.join([cwd, '..']), ()->{
                throw new Exception('failure');
            });
        }
        catch(e){}

        assertThat(Sys.getCwd(), is(equalTo(cwd)),
            'Should return to previous directory even with failure'
        );
    }

    @Test
    public function set_cwd() {
        sys.withCwd(Path.join([sys.getCwd(), '..']), ()->{

            var newDir = FileSystem.fullPath(Path.join([sys.getCwd(), '..']));
            sys.setCwd(newDir);

            assertThat(FileSystem.fullPath(Sys.getCwd()), is(equalTo(newDir)),
                'should change to specified directory'
            );
        });
    }

    @Test
    public function start_process() {
        var msg = 'Hello';
        var p = sys.startProcess('echo', [msg]);

        assertThat(p, is(notNullValue()), 'should create a valid process');
        assertThat(p, is(instanceOf(StdProcess)), 'should create a StdProcess');

        var output = p.stdout.readAll().toString();

        assertThat(output, is(equalTo(msg + '\n')));
    }

    @Test
    public function has_files() {
        assertThat(sys.files(), isA(Files), 'Should have files instance');
    }

    @Test
    public function environment() {
        sys.environment().map_is_equal_to(Sys.environment());
    }


    @Test
    public function put_and_get_env() {
        var key = 'example', value = Random.string(5);

        sys.putEnv(key, value);

        assertThat(sys.getEnv(key), equalTo(value),
            'Get key $key should be $value');
    }
}