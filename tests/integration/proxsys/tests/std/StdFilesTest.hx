package proxsys.tests.std;

import proxsys.Files.RemoveMode;
import proxsys.exceptions.FailedToRemoveException;
import proxsys.exceptions.PathNotFoundException;
import massive.munit.Assert;
import haxe.io.Path;
import sys.io.File;
import haxe.io.BytesBuffer;
import haxe.io.Bytes;
import haxe.Exception;
import sys.FileSystem;
import proxsys.std.StdFiles;

import Random;
import org.hamcrest.Matchers.*;

using StringTools;


class StdFilesTest {
    var files:Files;
    var tempFilepaths:Array<String>;

    public function new() {
    }

    @Before
    public function setup() {
        files = new StdFiles();
        tempFilepaths = [];
    }

    @After
    public function teardown() {
        for (f in tempFilepaths){
            try {
                removePath(f);
            }
            catch(e)
            {
                trace('failed to remove temporary path $f: $e');
            }
        }
    }

    function removePath(path:String) {
        files.remove(path, RemoveMode.Recursive);
        checkExist(path, false);
    }

    @Test
    public function exists_file() {
        checkExist('.', true);
        checkExist('this_file_should_not_exist', false);
    }


    @Test
    public function is_directory() {
        assertThat(files.isDirectory('.'), is(true),
            'Current path should be a directory');
    }

    @Test
    public function absolutePath() {
        assertThat(files.absolutePath('.'), equalTo(FileSystem.absolutePath('.')),
            'absolute path of "."');
    }

    @Test
    public function readDirectory() {
        assertThat(files.readDirectory('.'), equalTo(FileSystem.readDirectory('.')));
    }

    @Test
    public function save_and_read_content() {
        var path = tempFilename();
        var content = Random.string(50);

        files.saveContent(path, content);

        checkFileContent(path, content);
    }

    function checkFileContent(path:String, content:String) {
        var loadedContent = files.getContent(path);

        assertThat(loadedContent, equalTo(content),
            'Loaded content does not match expectation');
    }

    @Test
    public function save_and_read_bytes() {
        var path = tempFilename();
        var content = randomBytes(50);

        files.saveBytes(path, content);

        var loaded = files.getBytes(path);

        assertThat(loaded.toHex(), equalTo(content.toHex()),
            'Loaded bytes should be equalt to saved bytes'
        );
    }

    @Test
    public function get_file_stat() {
        // Given
        var size = 50;
        var path = givenFile(Random.string(size));

        // When
        var stat = files.stat(path);

        // Then
        assertThat(stat, is(notNullValue()), 'Stat should not be null');
        assertThat(stat.size, equalTo(size), 'Size in stat does not match');

        for (field in Reflect.fields(stat)){
            assertThat(Reflect.field(stat, field), is(notNullValue()),
                'Field $field of file stat should not be null'
            );
        }
    }

    @Test
    public function rename_file() {
        // Given
        var content = Random.string(Random.int(10, 50));
        var path = givenFile(content);
        var newPath = tempFilename();

        // When
        files.rename(path, newPath);

        // Then
        checkExist(newPath, true);
        checkExist(path, false);
        checkFileContent(newPath, content);
    }

    @Test
    public function rename_dir() {
        // Given
        var srcDir = givenDirectory();

        var content = Random.string(15);
        var srcFile = givenFileAt(srcDir, content);
        var newPath = tempFilename();

        // When
        files.rename(srcDir, newPath);

        // Then
        checkExist(newPath, true);
        checkExist(srcDir, false);

        var expectedNewFile = srcFile.replace(srcDir, newPath);
        checkFileContent(expectedNewFile, content);
    }

    @Test
    public function rename_nonexistent() {
        // Given
        var path = tempFilename();
        var newPath = tempFilename();

        // When+Then
        Assert.throws(
            PathNotFoundException, ()->{
                files.rename(path, newPath);
            }
        );
    }

    @Test
    public function remove_file() {
        testRemove(givenFile(), {expected: true});
    }

    @Test
    public function remove_empty_dir() {
        testRemove(givenDirectory(), {expected: true});
    }

    @Test
    public function remove_should_ignore_non_existent_path() {
        testRemove(tempFilename(), {expected: false});
    }

    @Test
    public function fail_to_remove_non_empty_dir_without_recursion() {
        var dirPath = givenDirectory();
        givenFileAt(dirPath);

        Assert.throws(
            FailedToRemoveException,
            ()->files.remove(dirPath)
        );

        checkExist(dirPath, true);
    }

    @Test
    public function remove_directory_recursive() {
        var dirPath = givenDirectory();
        var filePath = givenFileAt(dirPath);

        // Then
        testRemove(dirPath, {expected: true, recursive: true});

        // And
        checkExist(filePath, false);
    }

    function testRemove(path:String, options:{expected:Bool, ?recursive:Bool}) {
        var recursive  = options.recursive != null && options.recursive;
        var removeMode = recursive ? RemoveMode.Recursive : RemoveMode.Single;

        // When
        var removed = files.remove(path, removeMode);

        // Then
        checkExist(path, false);

        assertThat(removed, is(options.expected),
            'When removing $path return was not the expected');
    }

    // -------------------------------------------------------------

    function givenFile(?content:String, ?filepath:String): String {
        if(content == null){
            content = Random.string(Random.int(5, 50));
        }

        var path = filepath != null ? filepath : tempFilename();
        File.saveContent(path, content);

        return path;
    }

    function givenFileAt(dirPath:String, content:String='') {
        return givenFile(content, tempFilename(dirPath));
    }

    function givenDirectory(): String {
        var srcDir = tempFilename();
        FileSystem.createDirectory(srcDir);

        return srcDir;
    }

    function checkExist(path:String, shouldExist:Bool) {
        var should = shouldExist ? 'should' : 'should not';

        assertThat(files.exists(path), is(shouldExist),
            'Path "$path" $should exist');
    }

    function tempFilename(?baseDir:String) {
        var path = Random.string(10);

        if(baseDir != null){
            path = Path.join([baseDir, path]);
        }

        for (i in 0...10){
            if(!FileSystem.exists(path)){
                this.tempFilepaths.push(path);
                return path;
            }
            path += Random.string(1);
        }

        throw new Exception('Failed to find random path');
    }

    function randomBytes(length:Int): Bytes{
        var buffer = new BytesBuffer();

        for (i in 0...length){
            buffer.addByte(Random.int(0, 255));
        }

        return buffer.getBytes();
    }
}