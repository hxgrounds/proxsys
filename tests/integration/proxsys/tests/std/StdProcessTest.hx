package proxsys.tests.std;

import haxe.io.Output;
import haxe.io.Encoding;
import proxsys.std.StdProcess;

import org.hamcrest.Matchers.*;


class StdProcessTest {
    public function new() {}

    @Test
    public function create_process() {
        var p = startProcess('haxe', ['--version']);

        assertThat(p.stdout, is(notNullValue()),
            'Should have stdout');
        assertThat(p.stdin, isA(Output), 'Should have a valid stdin');

        assertThat(p.getPid(), is(greaterThan(0)),
            "Should have pid value");
    }

    @Test
    public function read_all_stdout() {
        var output = 'Hello world!';
        var p = startProcess('echo', [output]);

        var received = p.stdout.readAll().toString();

        assertThat(received, is(equalTo(output + '\n')),
            'Readed output does not match');
    }

    @Test
    public function write_line_to_stdin() {
        var p = startSample('proxsys.samples.EchoStdin');

        var text = "Hello World!";
        p.stdin.writeString(text + '\n', Encoding.UTF8);

        var line = p.stdout.readLine();

        assertThat(line, equalTo(text));

        p.stdin.close();
    }

    @Test
    public function wait_normal_exit() {
        var p = startSample('proxsys.samples.EchoStdin');

        #if !(neko)
            //Neko does not support non blocking check of exitCode
            assertThat(p.exitCode(false), is(nullValue()),
                "Should not have exited yet");
        #end

        p.stdin.close();

        assertThat(p.exitCode(true), is(equalTo(0)),
            "Should have closed normally");
    }


    @Test
    public function kill_the_process() {
        var p = startSample('proxsys.samples.EchoStdin');

        p.kill();

        assertThat(p.exitCode(), is(notNullValue()));
    }

    function startSample(sample: String) {
        return startProcess('haxe', ['-cp', '.', '--main', sample, '--interp']);
    }

    function startProcess(cmd: String, ?args: Array<String>): ProcessInstance{
        return new StdProcess(cmd, args);
    }
}