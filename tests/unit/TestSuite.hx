import massive.munit.TestSuite;

import proxsys.tests.fakes.FakeProcessTest;
import proxsys.tests.fakes.FakeFilesTest;
import proxsys.tests.fakes.system.FakeSystemFilesTest;
import proxsys.tests.fakes.system.FakeSystemEnvTest;
import proxsys.tests.fakes.system.FakeSystemProcessesTest;
import proxsys.tests.fakes.CommandMatcherTest;
import proxsys.tests.node.WritableOutputTest;

/**
 * Auto generated Test Suite for MassiveUnit.
 * Refer to munit command line tool for more information (haxelib run munit)
 */
class TestSuite extends massive.munit.TestSuite
{
	public function new()
	{
		super();

		add(proxsys.tests.fakes.FakeProcessTest);
		add(proxsys.tests.fakes.FakeFilesTest);
		add(proxsys.tests.fakes.system.FakeSystemFilesTest);
		add(proxsys.tests.fakes.system.FakeSystemEnvTest);
		add(proxsys.tests.fakes.system.FakeSystemProcessesTest);
		add(proxsys.tests.fakes.CommandMatcherTest);
		add(proxsys.tests.node.WritableOutputTest);
	}
}
