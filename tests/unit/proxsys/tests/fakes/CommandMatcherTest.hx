package proxsys.tests.fakes;

import org.hamcrest.Matchers.*;

import proxsys.fakes.CommandMatcher;
import proxsys.fakes.command.CommandSpec;
import proxsys.fakes.command.CommandInstance;

class CommandMatcherTest {
    var matcher:CommandMatcher<String>;
    var procId:Int;

    public function new() {}

    @Before
    public function setup() {
        matcher = new CommandMatcher();
        procId = 0;
    }

    @Test
    public function get_missing() {
        assertThat(matcher.get('cmd'), is(nullValue()));
    }

    @Test
    public function get_default() {
        var defaultValue = 'procDefault';
        assertThat(matcher.get('cmd', defaultValue), is(defaultValue));
    }

    @Test
    public function get_default__when_there_is_no_match() {
        var defaultValue = 'procDefault';
        matcher.push('proc', 'cmd', ['-a']);

        assertThat(matcher.get('cmd', ['-b'], defaultValue), is(defaultValue));
    }

    @Test
    public function get_by_command() {
        var command = 'example';

        matcher.push(command, 'cmd');

        assertThat(matcher.get('cmd'), is(command),
            'should retrieve instance with matching command');
    }

    @Test
    @:access(proxsys.fakes.CommandMatcher.instances)
    public function get_in_order() {
        var cmd = 'cmd';
        var cmds = givenSomeCommands(cmd, [
            ['a'],
            ['b'],
            ['a'],
            ['b'],
            ['a', 'b']
        ]);

        check_match(cmd, ['a'], null, cmds[0].instance,
            'should get matching process in order');
        check_match(cmd, ['b'], null, cmds[1].instance,
            'should get matching process in order');
        check_match(cmd, ['a', 'b'], null, cmds[4].instance,
            'should get matching process in order');
    }

    @Test
    public function get_with_default_matcher() {
        this.matcher = new CommandMatcher(CommandMatcher.matchBaseCommand);

        var cmds = givenSomeCommands('cmd', [
            null,
            ['a'],
            ['b']
        ]);

        check_match('cmd', ['a'], null, cmds[1].instance,
            'should prefer exact match');
        check_match('cmd', ['x'], null, cmds[0].instance,
            'should apply default matcher if no exact match is found');
    }
    @Test
    public function retrieve_using_custom_matcher() {
        this.matcher = new CommandMatcher((stored, query)->{
            return stored.cmd == query.cmd && stored.args.contains('default');
        });

        var otherCommands = givenSomeCommands('otherCmd', [['a', 'b']]);
        var cmds = givenSomeCommands('cmd', [
            ['-a'],
            ['-b', '-c'],
            ['-c', 'e'],
            ['default'],
            ['default', '-c']
        ]);

        var with_two_args = (stored:CommandSpec) -> {
            return stored.args.length == 2;
        }

        check_match('cmd', null, with_two_args, cmds[1].instance,
            'With custom matcher, should retrieve first match of given command'
        );
        check_match('cmd', ['-c', 'e'], with_two_args, cmds[2].instance,
            'With custom matcher, should filter also by args if specified'
        );
        check_match('cmd', ['-c', 'x'], with_two_args, cmds[4].instance,
            'With custom matcher, when there is no exact match, retrieve match using default matcher'
        );
    }


    @Test
    public function filter_with_args() {
        var proc = 'exampleProcess';
        var cmd = 'cmd';
        var args = ['-a'];

        matcher.push(proc, cmd, args);

        assertThat(matcher.get(cmd), is(null),
            'should not retrieve instance when args does not match');
    }

    @Test
    public function push_multiple_processes() {
        var procArgs = [['-a'], ['-b'], null, ['-c']];
        var procs    = [for (i in 0...procArgs.length) 'proc${i + 1}'];
        var cmd = 'cmd';

        for (i in 0...4){
            matcher.push(procs[i], cmd, procArgs[i]);
        }

        for (i in 0...1){
            var found = matcher.get(cmd, procArgs[i]);
            assertThat(found, is(procs[i]),
                'should store and retrieve matching processes with different args.\n'
                + '\t' + 'Expected: ${procs[i]} with ${procArgs[i]} but found $found');
        }

        assertThat(matcher.get(cmd), is(procs[2]),
            'match process with no args');
        assertThat(matcher.get(cmd, procArgs[3]), is(procs[3]),
            'match process with args');
    }


    @Test
    public function pop_process() {
        var proc = 'proc';
        var cmd = 'cmd';

        matcher.push(proc, cmd);

        assertThat(matcher.pop(cmd), is(proc),
            'pop should retrieve the right process'
        );
        assertThat(matcher.get(cmd), is(nullValue()),
            'pop should remove retrieved content');
    }


    @Test
    public function pop_process_with_matcher() {
        var cmds = givenSomeCommands('cmd', [
            ['a', 'b'],
            ['c', 'd', 'e'],
            ['d', 'e']
        ]);

        var has_d = (cmd:CommandSpec)->cmd.args.contains('d');

        assertThat(matcher.pop('cmd', has_d), equalTo(cmds[1].instance));
        assertThat(matcher.pop('cmd', has_d), equalTo(cmds[2].instance));
    }

    @Test
    public function pop_process_should_return_matching_processes_by_order() {
        var procs = [for(i in 0...5) 'proc${i+1}'];
        var cmd = 'cmd';

        for(p in procs) matcher.push(p, cmd);

        for(i in 0...procs.length){
            var found = matcher.pop(cmd);

            assertThat(found, is(procs[i]),
                'pop should retrieve the matching processes in order. Failed at index $i');
        }
    }


    @Test
    public function match_instance() {
        assertThat(new CommandInstance('any', 'cmd').match('cmd'), is(true),
            'Match by command name');
        assertThat(new CommandInstance('any', 'cmd').match('cmd', ['1', '2']), is(true),
            'Command with null args should match same command name with any args');
        assertThat(new CommandInstance('any', 'cmd', ['-a', '1']).match('cmd'), is(false),
            'Command with args should not match same command name with no args');
        assertThat(new CommandInstance('any', 'cmd', ['-a']).match('cmd', ['-b']), is(false),
            'Command with different args should not match');
    }

    // Helpers -------------------------------------------


    function givenSomeCommands(cmd: String, listOfArgs: Array<Array<String>>) {
        var instances = [
            for(args in listOfArgs) new CommandInstance('proc${procId++}', cmd, args)
        ];

        for (inst in instances)
            matcher.push(inst.instance, inst.cmd, inst.args);

        return instances;
    }

    function check_match(
        cmd:String, args:Array<String>, customMatcher:Null<CustomMatcher>, proc:String, ?message:String)
    {
        if(message == null)
            message = 'When getting command with ($cmd, $args, $customMatcher) should return expected process';

        var found = matcher.get(cmd, args, customMatcher);

        assertThat(found, is(equalTo(proc)), message);
    }
}