package proxsys.tests.fakes;

import proxsys.Files.RemoveMode;
import haxe.PosInfos;
import proxsys.exceptions.FailedToRemoveException;
import proxsys.exceptions.PathNotFoundException;
import massive.munit.Assert;
import proxsys.fakes.files.FileStatBuilder;
import org.hamcrest.Matchers;
import haxe.io.Path;
import haxe.io.Bytes;
import proxsys.fakes.FakeFiles;

import org.hamcrest.Matchers.*;


using proxsys.fakes.assertions.FakeFilesAssertions;
using DateTools;

class FakeFilesTest {
    var fakeFiles:FakeFiles;

    public function new() {
    }

    @Before
    public function setup() {
        fakeFiles = new FakeFiles();
    }

    @Test
    public function absolutePath() {
        fakeFiles.setCwd("/a");

        testAbsolutePath("local", "/a/local");
        testAbsolutePath("/absolute", "/absolute");
    }

    @Test
    public function testCwd() {
        assertThat(fakeFiles.getCwd(), is(equalTo("/")), "default cwd");

        checkGetCwd("/abs", "/abs");
        checkGetCwd("sub", "/abs/sub");
        checkGetCwd("/a", "/a");
    }

    function checkGetCwd(newCwd:String, expected:String) {
        var previous = fakeFiles.getCwd();

        fakeFiles.setCwd(newCwd);
        assertThat(fakeFiles.getCwd(), is(equalTo(expected)),
            'Setting cwd to "$newCwd" from "$previous", then new cwd should be: "$expected"');
    }

    @Test
    public function exists_using_unix_paths() {
        testExists("local", FileType);
        testExists("dir", DirectoryType);
        testExists("/absolute", FileType);
        testExists("/a/b", FileType, "/a");
        testExists("/x/y/z", FileType, "/x");
        testExists("/normalize/", FileType, "/normalize");
        testExists("/normalize2", FileType, "/normalize2/");

        fakeFiles.setCwd("/a");
        testExists("local", FileType,  "/a/local");
    }

    @Test
    public function exists_using_windows_paths() {
        fakeFiles.setCwd('C:\\');

        testExists("local", FileType);
        testExists("dir", DirectoryType);
        testExists("C:\\absolute", FileType);
        testExists("D:\\a\\b", FileType, "D:\\a");
        testExists("C:\\x\\y\\z", FileType, "C:\\x");
        testExists("C:\\normalize\\", FileType, "C:\\normalize");
        testExists("C:\\normalize2", FileType, "C:\\normalize2\\");

        fakeFiles.setCwd("D:\\a");
        testExists("local", FileType,  "D:\\a\\local");
    }

    @Test
    public function isDirectory() {
        fakeFiles.putPath('/a', DirectoryType);
        fakeFiles.putPath('/b/c', FileType);

        fakeFiles.shouldBeADirectory('/a', true);
        fakeFiles.shouldBeADirectory('/b/c', false);
        fakeFiles.shouldBeADirectory('/b', true);
    }

    @Test
    public function readDirectory() {
        var putFiles = ['a', 'b/c'];
        var expectedFiles = ['a', 'b'];

        test_read_directory('/example/dir', putFiles, expectedFiles);
        test_read_directory('local/dir', putFiles, expectedFiles);
        test_read_directory('C:\\win\\dir', putFiles, expectedFiles);
        test_read_directory('local\\windir', putFiles, expectedFiles);

        test_read_directory('/x/y', ['c/d', 'e/f'], ['c', 'e']);

        fakeFiles.putPath('/g/h/i/j', FileType);
        fakeFiles.putPath('/g/h/i/k', FileType);
        test_read_directory('/g/h/i', [], ['j', 'k']);
    }

    function test_read_directory(
        dirPath:String, putFiles:Array<String>, ?expectedFiles:Array<String>)
    {
        fakeFiles.putPath(dirPath, DirectoryType);
        for (f in putFiles){
            fakeFiles.putPath(Path.join([dirPath, f]), FileType);
        }

        var readedFiles = fakeFiles.readDirectory(dirPath);
        readedFiles.sort((a, b)->Reflect.compare(a, b));

        assertThat(readedFiles, is(array(expectedFiles)),
            'Readed files does not match');
    }

    @Test
    public function removePath() {
        fakeFiles.putPath('/a/b/c/d', FileType);
        fakeFiles.putPath('/a/b/e/f', FileType);
        fakeFiles.putPath('/a/h/i', FileType);
        fakeFiles.putPath('/a/j/k', FileType);

        fakeFiles.remove('/a/b/c/d');
        fakeFiles.remove('/a/b', RemoveMode.Recursive);
        fakeFiles.removePath('/a/j'); //equivalent to remove recursive

        fakeFiles.pathShouldExist('/a/b/c/d', false);
        fakeFiles.pathShouldExist('/a/b', false);
        fakeFiles.pathShouldExist('/a/b/c', false);
        fakeFiles.pathShouldExist('/a/b/e', false);
        fakeFiles.pathShouldExist('/a/b/e/f', false);
        fakeFiles.pathShouldExist('/a', true);
        fakeFiles.pathShouldExist('/a/h', true);
        fakeFiles.pathShouldExist('/a/h/i', true);
        fakeFiles.pathShouldExist('/a/j', false);
        fakeFiles.pathShouldExist('/a/j/k', false);
    }

    @Test
    public function fail_to_remove_dir_non_recursive() {
        fakeFiles.putPath('/a/b/c/d', FileType);

        failToRemove('/a');
        failToRemove('/a/b');
        failToRemove('/a/b/c');
    }

    function failToRemove(path:String) {
        Assert.throws(
            FailedToRemoveException,
            ()->fakeFiles.remove(path)
        );
    }

    @Test
    public function remove_should_return_if_file_was_removed() {
        fakeFiles.putPath('/a/b/c/d', FileType);

        test_remove('/a/b/c/d', { expected: true });
        test_remove('/a/b/c', { expected: true });
        test_remove('x', { expected: false });
        test_remove('/a/b/c/d', { expected: false });
    }

    function test_remove(path:String, options:{expected:Bool}, ?pos:PosInfos) {
        var removed = fakeFiles.remove(path);

        assertThat(removed, is(options.expected),
            'When removing $path return does not match expectation', pos);
    }

    @Test
    public function save_and_get_bytes() {
        var bytes = Bytes.ofHex('f241b6e615b359ffa88e73d7ca7c4f');
        var filename = "example";

        fakeFiles.saveBytes(filename, bytes);
        var gotBytes = fakeFiles.getBytes(filename);

        fakeFiles.pathShouldExist(filename);

        assertThat(gotBytes, is(notNullValue()),
            'Got bytes should not be null');
        assertThat(gotBytes.toHex(), equalTo(bytes.toHex()),
            'Got bytes should be equal to saved bytes');
    }

    @Test
    public function get_file_stat() {
        //Given
        var before = Date.now();
        var path = givenFile();

        //When
        var fileStat = fakeFiles.stat(path);

        //Then
        assertThat(fileStat, is(notNullValue()),
            'Created file should haave a filestat');

        should_be_a_date_between(fileStat.ctime, before, Date.now(), 'creation time');
        should_be_a_date_between(fileStat.atime, before, Date.now(), 'access time');
        should_be_a_date_between(fileStat.mtime, before, Date.now(), 'modified time');

        assertThat(fileStat.size, is(0), 'Size is of file');
    }

    @Test
    public function set_file_stat() {
        var path = givenFile();

        var fileStat = FileStatBuilder.builder()
                        .ctime(Date.now().delta(-10))
                        .build();

        fakeFiles.setStat(path, fileStat);

        var gotStat = fakeFiles.stat(path);

        assertThat(gotStat, equalTo(fileStat),
            'Should have set filestat'
        );
    }


    @Test
    public function rename_file() {
        // Given
        var srcPath = '/example/path';
        var dstPath = '/example/newpath';
        var content = "Lorem ipsum dolor sit amet";

        fakeFiles.saveContent(srcPath, content);

        // When
        fakeFiles.rename(srcPath, dstPath);

        // Then
        fakeFiles.pathShouldExist(dstPath, true);
        fakeFiles.pathShouldExist(srcPath, false);

        assertThat(fakeFiles.getContent(dstPath), equalTo(content),
            '$dstPath content should be $content');
    }

    @Test
    public function rename_dir() {
        // Given
        fakeFiles.putPath('/a/b/c/d', FileType);

        var content = "Example";
        fakeFiles.saveContent('/a/b/c/e', content);

        // When
        fakeFiles.rename("/a/b", "/a/x");

        // Then
        fakeFiles.pathShouldExist('/a/b', false);
        fakeFiles.pathShouldExist('/a/b/c', false);
        fakeFiles.pathShouldExist('/a/b/c/d', false);
        fakeFiles.pathShouldExist('/a/b/c/e', false);
        fakeFiles.pathShouldExist('/a/x');
        fakeFiles.pathShouldExist('/a/x/c');
        fakeFiles.pathShouldExist('/a/x/c/d');
        fakeFiles.pathShouldExist('/a/x/c/e');

        assertThat(fakeFiles.getContent('/a/x/c/e'), equalTo(content),
            'Renamed file content should be $content');
    }

    @Test
    public function rename_inexistent_file() {
        Assert.throws(
            PathNotFoundException,
            ()->fakeFiles.rename("not/found/path", "other/path")
        );
    }

    // -------------------------------------------------------------------------

    function testAbsolutePath(relPath: String, expected:String) {
        assertThat(fakeFiles.absolutePath(relPath), is(equalTo(expected)),
            'Absolute path of "$relPath" should be $expected');
    }

    function testExists(
        putPath: String, pathType: FileTypes = FileType,
        ?checkPath: String, exists:Bool = true)
    {
        if(checkPath == null){
            checkPath = putPath;
        }

        fakeFiles.putPath(putPath, pathType);

        fakeFiles.pathShouldExist(checkPath, exists);
    }

    // -----------------------------------------------------

    function givenFile() {
        var path = '/example/path';

        fakeFiles.putPath(path);

        return path;
    }


    function should_be_a_date_between(date:Date, before:Date, after:Date, msg:String) {
        assertThat(date, is(notNullValue()), msg);
        assertThat(date.getTime(),
            is(both(lessThanOrEqualTo(after.getTime()))
                .and(greaterThanOrEqualTo(before.getTime()))), msg);
    }
}