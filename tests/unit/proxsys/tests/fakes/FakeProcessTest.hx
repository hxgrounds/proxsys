package proxsys.tests.fakes;

import proxsys.tools.ProcessTools;
import proxsys.fakes.FakeProcess;
import org.hamcrest.Matchers.*;

class FakeProcessTest {

    @Before
    public function setup() {
    }

    @Test
    public function readAllText() {
        testReadAllText('123');
        testReadAllText('line 1\nline 2');
        testReadAllText('line 1\nline 2\n');
    }

    @Test
    public function getPid() {
        var proc = new FakeProcess();

        assertThat(proc.getPid(), is(greaterThan(0)),
            'Default pid should be a positive number');
    }

    @Test
    public function set_and_get_exit_code() {
        var exitCode = Std.random(1001) -500;
        var proc = new FakeProcess();
        proc.setExitCode(exitCode);

        assertThat(proc.exitCode(), is(exitCode),
            'If exit code is set should return value');
    }

    @Test
    public function get_unset_exit_code_non_blocking() {
        var proc = new FakeProcess();

        assertThat(proc.exitCode(false), is(null),
            'If exit code is not set should return null when getting nonblock');
    }

    @Test
    public function get_exit_code_blocking() {
        var proc = new FakeProcess();

        assertThat(proc.exitCode(true), is(0),
            'If exit code is unset, when blocking until exit code, should return 0');
        assertThat(proc.exitCode(false), is(0),
            'After first time, should still return 0 when getting exit code non blocking');
    }

    @Test
    public function execute_process() {
        var givenCommand = 'cmd';
        var givenArgs = ['a1', 'b1'];

        var called = false;
        var capturedCmd: String;
        var capturedArgs:Array<String>;

        var proc = new FakeProcess().onCall((cmd, args)->{
            called = true;
            capturedCmd = cmd;
            capturedArgs = args;
        });

        proc.execute(givenCommand, givenArgs);

        assertThat(called, is(true), 'Command listener should be called');
        assertThat(capturedCmd, is(givenCommand), 'Command does not match');
        assertThat(capturedArgs, is(givenArgs), 'Command args does not match');
    }

    @Test
    public function kill_process() {
        var proc = new FakeProcess();

        //When:
        proc.kill();

        assertThat(proc.wasKilled, is(true),
            'should register if process was killed');
    }

    @Test
    public function clone_process() {
        var output = 'Lorem ipsum';
        var input = 'Example';

        var proc = new FakeProcess()
                    .output(output)
                    .setExitCode(1);
        proc.stdout.read(1);
        proc.stdin.writeString(input);
        proc.kill();

        var cloned:FakeProcess = proc.clone();

        assertThat(cloned.exitCode(), equalTo(proc.exitCode()),
            'cloned exitCode should match');
        assertThat(cloned.stdout.readAll().toString(), equalTo(output),
            'cloned output should match');
        assertThat(cloned.writtenBytes().toString(), equalTo(input),
            'cloned input should match');
        assertThat(cloned.wasKilled, is(equalTo(proc.wasKilled)),
            'Killed status should be cloned');
    }

    // ------------------------------------------------------------------------

    function testReadAllText(text:String) {
        var proc = new FakeProcess();
        proc.output(text);

        var readedText = ProcessTools.readAllText(proc);

        assertThat(readedText, is(equalTo(text)),
            'Should read all informed text');
    }
}