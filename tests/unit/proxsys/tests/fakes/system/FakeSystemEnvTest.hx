package proxsys.tests.fakes.system;

import org.hamcrest.Matchers.*;
import Random;

using proxsys.tests.SystemAssertions;


class FakeSystemEnvTest {
    var system:FakeSystem;

    public function new() {
    }

    @Before
    public function setup() {
        system = new FakeSystem();
    }

    @Test
    public function put_and_get_env() {
        var key = 'example';
        var value = 'lorem ipsum';

        system.putEnv(key, value);

        assertThat(system.getEnv(key), equalTo(value),
            'Get value should match put value');
    }


    @Test
    public function put_and_get_environment() {
        var expected = randomEnv();

        system.putEnvironment(expected);
        var env = system.environment();

        env.keys_should_be_equal_to(expected.keys());
        system.environment_should_contain(expected);
    }

    @Test
    public function get_environment_should_be_immutable() {
        givenEnvironment();
        var newKey = 'new';

        var env = system.environment();
        env.set(newKey, Random.string(5));

        assertThat(system.getEnv(newKey), is(nullValue()),
            'Changes in returned environment should not change system environment');
    }

    @Test
    public function get_env_should_synchronize_with_get_environment() {
        var env = givenEnvironment();

        system.environment_should_contain(env);
    }

    @Test
    public function put_environment_should_append_existing_variables() {
        var key = 'my_variable';
        var value = Random.string(5);
        system.putEnv(key, value);

        var env = randomEnv();
        system.putEnvironment(env);

        system.environment_should_contain(env);
        system.environment_should_contain([key => value]);
    }

    // ----------------------------------------------------------------

    function givenEnvironment() {
        var env = randomEnv();
        system.putEnvironment(env);

        return env;
    }

    // ----------------------------------------------------------

    function randomEnv() {
        var map = new Map<String, String>();

        for (i in 0...10){
            map.set('env_$i', Random.string(8));
        }

        return map;
    }
}