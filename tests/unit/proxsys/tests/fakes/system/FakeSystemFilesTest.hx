package proxsys.tests.fakes.system;

import proxsys.fakes.FakeFiles;
import org.hamcrest.Matchers.*;

class FakeSystemFilesTest {

    var sys:System;

    public function new() {}

    @Before
    public function setup() {
        sys = new FakeSystem();
    }

    @Test
    public function has_files() {
        assertThat(sys.files(), isA(FakeFiles));
    }

    @Test
    public function cwd_should_be_synchronized() {
        var fakeFiles = givenFakeFiles();
        var pathA = '/override/path';
        var pathB = '/override/path';

        sys.setCwd(pathA);
        assertCwdPathsAre(pathA);

        fakeFiles.setCwd(pathB);
        assertCwdPathsAre(pathB);
    }

    function assertCwdPathsAre(path:String) {
        assertThat(sys.getCwd(), is(equalTo(path)), 'sys.getCwd()');
        assertThat(givenFakeFiles().getCwd(), is(equalTo(path)), 'fakeFiles.getCwd()');
    }

    // --------------------------------------------------

    function givenFakeFiles() {
        return cast(sys.files(), FakeFiles);
    }
}