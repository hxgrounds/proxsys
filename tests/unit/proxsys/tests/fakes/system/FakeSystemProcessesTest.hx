package proxsys.tests.fakes.system;

import proxsys.fakes.command.CommandSpec;
import proxsys.fakes.command.CommandSpecMatcher;
import proxsys.fakes.FakeProcess;
import massive.munit.Assert;

import proxsys.FakeSystem;

import org.hamcrest.Matchers.*;


class FakeSystemProcessesTest
{
    var sys:FakeSystem;

    public function new()
    {
    }

    @Before
    public function setup() {
        sys = new FakeSystem();
    }

    @Test
    public function testCommandWithoutArgs()
    {
        var cmd = "example";

        sys.command(cmd);

        Assert.isNotNull(sys.lastCommand());
        Assert.areEqual(cmd, sys.lastCommand());
    }


    @Test
    public function testCommandWithArgs()
    {
        var cmd = "example";
        var args = ['a', 'b', 'c'];

        sys.command(cmd, args);


        Assert.areEqual(cmd + ' ' + args.join(' '), sys.lastCommand());
    }

    @Test
    public function executingProcess_should_return_fake_process_to_configure() {
        var p = sys.givenProcess('cmd');

        assertThat(p, is(notNullValue()), 'Process should not be null');
        assertThat(p, isA(FakeProcess), 'Process should be a FakeProcess');
    }

    @Test
    public function startProcess_should_return_valid_process_even_if_not_configured() {
        var p = sys.startProcess('cmd');

        assertThat(p, is(notNullValue()), 'Process should not be null');
        assertThat(p, isA(FakeProcess), 'Process should be a FakeProcess');
    }

    @Test
    public function match_configured_process() {
        should_be_same(sys.givenProcess('cmd'), sys.startProcess('cmd'),
            'Should match process with same command'
        );
        should_be_same(sys.givenProcess('cmd'), sys.startProcess('cmd', ['a', 'b']),
            'Should match process with same command but started with some arguments'
        );
        should_not_be_same(
            sys.givenProcess('cmd', ['-a']), sys.startProcess('cmd', ['-b']),
            'Should not match process with same command but different arguments'
        );
    }

    @Test
    public function a_configured_process_should_be_returned_once() {
        //Given
        var proc = sys.givenProcess('cmd');

        //When
        var startedProc1 = sys.startProcess('cmd');
        var startedProc2 = sys.startProcess('cmd');

        //Then
        should_be_same(proc, startedProc1,
            'At first match, should return configured process'
        );
        should_not_be_same(proc, startedProc2,
            'A configured process should be started once'
        );
    }


    @Test
    public function can_configure_an_equal_process_twice() {
        //Given
        var proc1 = sys.givenProcess('cmd');
        var proc2 = sys.givenProcess('cmd');

        //When
        var startedProc1 = sys.startProcess('cmd');
        var startedProc2 = sys.startProcess('cmd');

        //Then
        should_be_same(proc1, startedProc1,
            'startProcess should return first matching configured process');
        should_be_same(proc2, startedProc2,
            'startProcess should return the next matching configured process');
        should_not_be_same(proc1, proc2,
            'Multiple process configurations with same args should return different commands');
    }

    @Test
    public function default_process_config_should_be_persistent() {
        should_be_same(sys.defaultProcess(), sys.defaultProcess(),
            'Default process config should be persistent');
    }

    @Test
    public function default_process_config_should_be_used_with_given_processes() {
        var defaultProc = given_default_proc_is_configured();

        var proc = sys.givenProcess('any');

        process_should_not_be_the_same_of_default(proc, defaultProc);
        process_config_should_be_inherited_from_other(proc, defaultProc);
    }

    @Test
    public function default_process_config_should_be_used_with_started_processes() {
        var defaultProc = given_default_proc_is_configured();

        var proc = sys.startProcess('any');

        process_should_not_be_the_same_of_default(proc, defaultProc);
        process_config_should_be_inherited_from_other(cast(proc), defaultProc);
    }

    function given_default_proc_is_configured(
        exitCode:Int=100, output:String = 'lorem ipsum')
    {
        return sys.defaultProcess()
                                .setExitCode(exitCode)
                                .output(output);
    }

    inline function process_should_not_be_the_same_of_default(
        proc:ProcessInstance, defaultProc:ProcessInstance)
    {
        should_not_be_same(proc, defaultProc,
            'New process should not be the same of default process');
        assertThat(proc.getPid(), not(equalTo(defaultProc.getPid())),
            'Pid of new process should not be the same of default process');

    }

    inline function process_config_should_be_inherited_from_other(
        proc:FakeProcess, other:FakeProcess)
    {
        assertThat(proc.exitCode(), equalTo(other.exitCode()),
            'Default exit code should be passed to new process');
        assertThat(proc.getOutputText(), equalTo(other.getOutputText()),
            'Default output should be passed to new processes');
    }

    @Test
    public function check_executed_command() {
        var cmds = givenExecutedCommands('cmd', [
            ['a'],
            ['b', 'c'],
            ['c', 'd', 'e'],
            ['b', 'c'],
        ]);

        checkExecuted('cmd', ['a'], cmds[0]);
        checkExecuted('cmd', ['b', 'c'], cmds[1]);
        checkExecuted('cmd', null, cmds[0],
            'Getting command without args should return first execution');
        checkExecuted('cmd', null, cmds[2],
            (cmdSpec:CommandSpec)->cmdSpec.args.contains('d'),
            'Getting command with custom matcher');

    }

    @Test
    public function pop_executed_process() {
        var cmds = givenExecutedCommands('cmd', [
            ['a'],
            ['a'],
            ['b', 'c'],
            ['c', 'd', 'e'],
            ['b', 'c'],
            ['e']
        ]);

        checkExecuted('cmd', ['a'], cmds[0], true);
        checkExecuted('cmd', ['b', 'c'], cmds[2], true);
        checkExecuted('cmd', ['a'], cmds[1], true);
        checkExecuted('cmd', ['a'], null, true);
        checkExecuted('cmd', null, cmds[3], true);
        checkExecuted('cmd', null, cmds[5],
            (cmdSpec:CommandSpec)->cmdSpec.args.contains('e'), true,
            'Popping command with custom matcher');
    }

    @Test
    public function call_command_listener_on_execution() {
        var executed = false;

        sys.givenProcess('cmd').onCall((cmd, args)->{
            executed = true;
        });

        sys.startProcess('cmd');

        assertThat(executed, is(true), 'Command listener should be called');
    }

    // --------------------------------------------------------------------------

    function givenExecutedCommands(cmd: String, listOfArgs: Array<Array<String>>) {
        return[
            for(args in listOfArgs) cast(sys.startProcess(cmd, args), FakeProcess)
        ];
    }

    function checkExecuted(
        cmd: String, args: Array<String>, expected: ProcessInstance,
        ?matcher:CommandSpecMatcher,
        ?remove:Bool, ?msg:String)
    {
        var executed = if(remove != true)
                            sys.getExecuted(cmd, args, matcher)
                        else sys.popExecuted(cmd, args, matcher);

        if(msg == null)
            msg = 'Getting command ($cmd, $args) should retrieve $expected';

        should_be_same(expected, executed, msg);
    }

    // Helpers ------------------------------------------------------------

    function should_be_same(fakeProc:ProcessInstance, startedProc:ProcessInstance, msg:String) {
        assertThat(startedProc, is(fakeProc), msg);
    }

    function should_not_be_same(fakeProc:ProcessInstance, startedProc:ProcessInstance, msg:String) {
        assertThat(startedProc, not(is(fakeProc)), msg);
    }
}