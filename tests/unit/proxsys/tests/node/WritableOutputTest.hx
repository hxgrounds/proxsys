package proxsys.tests.node;

#if nodejs
import proxsys.fakes.util.FakeWait;
import proxsys.std.node.WritableOutput;
import proxsys.fakes.node.FakeWritable;

import org.hamcrest.Matchers.*;
#end

class WritableOutputTest {
    public function new() {
    }

#if nodejs
    var fakeWait:FakeWait;
    var fakeWritable:FakeWritable;

    var writeableOutput:WritableOutput;

    @Before
    public function setup() {
        fakeWait = new FakeWait();
        fakeWritable = new FakeWritable();

        writeableOutput = new WritableOutput(fakeWritable, fakeWait);
    }

    @Test
    public function before_writing_should_wait_for_drain_event__if_needed() {
        given_write_returned_false();

        when_writing('example');

        then_should_start_waiting_for_drain_event();
    }

    @Test
    public function should_continue_writing_after_drain_event() {
        given_started_waiting_for_drain();

        when_event_is_called('drain');

        then_wait_condition_should_change_to(true);
    }

    // steps -----------------------------------------------


    function given_started_waiting_for_drain() {
        this.given_write_returned_false();
        this.when_writing('anything');
    }

    function given_write_returned_false() {
        fakeWritable.nextWriteShouldReturn(false);

        when_writing('a');
    }

    function when_writing(s:String) {
        writeableOutput.writeString(s);
    }

    function when_event_is_called(event:String) {
        this.fakeWritable.emit(event);
    }

    function then_should_start_waiting_for_drain_event() {
        assertThat(fakeWait.lastWaitCondition, is(notNullValue()),
            'should have started waiting');

        this.then_wait_condition_should_be(false);
    }

    function then_wait_condition_should_change_to(value:Bool) {
        this.then_wait_condition_should_be(value);
    }
    function then_wait_condition_should_be(value:Bool) {
        assertThat(fakeWait.lastWaitCondition(), is(value),
            'wait condition should be $value');
    }
#end

}